from flask import Flask, jsonify, request
from flask_cors import CORS
import json
import os
import csv

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})


@app.route('/api/detect', methods=['POST'])
def backend():
    benchmarks = request.json["benchmarks"]
    tools = request.json["tools"]
    iterations = request.json["iter"]
    print(benchmarks, tools, iterations)

    if benchmarks[0] != "all":
        tmp = ""
        for i in benchmarks:
            tmp += i+":"
        benchmarks = tmp[:-1]
    else:
        benchmarks = "all"


    # os.chdir("/home/dell/Desktop/RoadRunner")
    # os.system("source ~/.bashrc")
    # os.system("ant")
    # exec(open("msetup1.5").read())
    # os.environ['JAVA_HOME']='/System/Library/Frameworks/JavaVM.framework/Versions/1.5/Home'
    # os.environ['JVM_ARGS']='-Xmx1g -Xms1g'
    # rrr = os.system('which rrrun')

    # print('RRUN -- - - - ', rrr)
    # os.environ['rrrun']=rrr

    # os.environ['RR_HOME']='/home/dell/Desktop/RoadRunner'
    # os.environ['RR_TOOLPATH']='/home/dell/Desktop/RoadRunner/classes/rr/simple:/home/dell/Desktop/RoadRunner/classes/tools'



    os.chdir("/home/dell/Desktop/RoadRunner/benchmarks")
    # reslist = []
    # if benchmarks == "all" and tools[0] == "all":

    #     # print("./TEST_FT2")
    #     os.system("./TEST_FT2")
    #     alltools = ["BE", "FT2", "SLplus"]
    #     for i in alltools:
    #         with open(i+'.csv', newline='') as f:
    #             reader = csv.reader(f)
    #             data = list(reader)
    #             reslist.append(data)
    # else:
    #     for i in tools:
    #         #print("./TEST_BMS "+iterations+" "+benchmarks+" "+i+" -quiet -maxWarn=1 -tool="+i)
    #         os.system("./TEST_BMS "+iterations+" "+benchmarks +
    #                   " "+i+" -quiet -maxWarn=1 -tool="+i)
    #         with open(i+'.csv', newline='') as f:
    #             reader = csv.reader(f)
    #             data = list(reader)
    #             reslist.append(data)

    reslist = []
    if benchmarks=="all" and tools[0]=="all":

        #print("./TEST_FT2")
        #os.system("./TEST_FT2")
        return_value = os.system("gnome-terminal -e 'bash -c \"cd /home/dell/Desktop/RoadRunner/; source msetup1.5; cd benchmarks; ./TEST_FT2\"'")
        os.system("sleep 60s")

        alltools = ["BE", "FT2", "SLplus"]
        for i in alltools:
            with open(i+'.csv', newline='') as f:
                reader = csv.reader(f)
                data = list(reader)
                reslist.append(data)
    else:
        if tools[0]=="all":
            tools = ["BE", "FT2", "SLplus"]
        for i in tools:
            #print("./TEST_BMS "+iterations+" "+benchmarks+" "+i+" -quiet -maxWarn=1 -tool="+i)
            #os.system("./TEST_BMS "+iterations+" "+benchmarks+" "+i+" -quiet -maxWarn=1 -tool="+i)
            TEST_BMS = "cd /home/dell/Desktop/RoadRunner/; source msetup1.5; cd benchmarks;"+"./TEST_BMS "+iterations+" "+benchmarks+" "+i+" -quiet -maxWarn=1 -tool="+i
            return_value = os.system("gnome-terminal -e 'bash -c \""+ TEST_BMS+"\"'")
            os.system("sleep 300s")

            with open(i+'.csv', newline='') as f:
                reader = csv.reader(f)
                data = list(reader)
                reslist.append(data)

    res = {"Result": reslist}

    print(res)
    return jsonify(res), 201


if __name__ == '__main__':
    app.run('0.0.0.0', port=8080, debug=True)
